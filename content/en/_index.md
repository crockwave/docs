---
title: Soapbox documentation
---

Welcome to the Soapbox documentation!

**Choose your path:**

- [Learn how to use Soapbox]({{< relref "usage/basics.md" >}})
- [Learn how to install Soapbox]({{< relref "administration/installation.md" >}})
- [Learn how to write an app for Soapbox]({{< relref "api/guidelines.md" >}})
