---
title: Dokumentacja Soapboxa
---

Witaj w dokumentacji Soapboxa!

**Wybierz swoją drogę:**

- [Dowiedz się jak korzystać z Soapboxa]({{< relref "usage/basics.md" >}})
- [Dowiedz się jak zainstalować Soapboxa]({{< relref "administration/installation.md" >}})
- [Dowiedz się jak napisać aplikację dla Soapboxa]({{< relref path="api/guidelines.md" lang="en" >}})
