# Soapbox Documentation

Live docs: https://docs.soapbox.pub

Soapbox docs are based on Mastodon's docs and built with [Hugo](https://gohugo.io/).

To see your changes locally, first install Hugo (Ubuntu: `sudo apt install hugo`) then run the Hugo server in the project directory:

```sh
hugo server
```

If you edit the SCSS, you will need to rebuild it manually:

```sh
scss public/style.scss public/style.css
```

The docs are licensed under CC BY-SA 4.0.
